import service.BasicOperations;

import java.util.Scanner;

public class CalculatorApp {
    public static void main(String[] args) {
        System.out.println("-=-=-=-=-=-=Start-=-=-=-=-=-=");
        System.out.println("Wprowadź pierwszą liczbę: ");
        Scanner scanner = new Scanner(System.in);
        Double firstValue = scanner.nextDouble();
        System.out.println("Wprowadź drugą liczbę: ");
        Double secondValue = scanner.nextDouble();

        Double result = BasicOperations.subtract(firstValue, secondValue);
        System.out.println("Wynik odejmowania: " + result);

    }
}
