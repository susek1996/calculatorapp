package service;

public class BasicOperations {

    public static double subtract(double a, double b){
        return a-b;
    }

    public static double addition(double a, double b){
        return a+b;
    }

    public static double dividing(double a, double b){
        double result = 0;
        try{
            result = a/b;
        }
        catch(ArithmeticException e){
            System.out.println("Nie dziel przez zero");
        }
        return result;
    }

    public static double multiplication(double a, double b){
        return a*b;
    }
}
